﻿using UnityEngine;
using System.Collections;

public class movement : MonoBehaviour {
	public float Movementspeed = 1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
			transform.Translate (Vector3.up*Movementspeed*Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.S)) {
			transform.Translate (Vector3.down*Movementspeed*Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.A)) {
			transform.Translate (Vector3.left*Movementspeed*Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.D)) {
			transform.Translate (Vector3.right*Movementspeed*Time.deltaTime);
		}
	}
}
