﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class death_trigger : MonoBehaviour {
	public string GameOver;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D (Collider2D other){
		if (other.tag == "Player") {
			SceneManager.LoadScene (GameOver); 
		}
	}
}
