﻿using UnityEngine;
using System.Collections;

public class EnemyDumb : MonoBehaviour
{

    public float stdFlip = -1;

    public float velocity = 2f;

    public Transform sightstart;
    public Transform sightEnd;

    Rigidbody2D rb2d;

    public float timeMovementRight = 5f;
    public float timeMovementLeft = 5f;

    public bool moveRight;
    public bool moveLeft;

    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        rb2d.velocity = new Vector2(velocity, rb2d.velocity.y);

        if (transform.position.x >= sightstart.transform.position.x)
        {
            moveLeft = true;
            moveRight = false;
            this.transform.localScale = new Vector3(stdFlip, this.transform.localScale.y, this.transform.localScale.z);
        }
        if (transform.position.x <= sightEnd.transform.position.x)
        {
            moveRight = true;
            moveLeft = false;
            this.transform.localScale = new Vector3(stdFlip * -1, this.transform.localScale.y, this.transform.localScale.z);
        }

        if (moveRight)
        {
			velocity = velocity;
        }
        else
        {
            //moveLeft = true;
			velocity = -velocity;
        }


    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(sightstart.position, sightEnd.position);
    }

    //void OnTriggerEnter2D(Collider2D other)
   // {
		//if (other.tag == "Player") {
			//Destroy(gameObject);
		//}
   // }
}
