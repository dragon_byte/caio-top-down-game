﻿using UnityEngine;
using System.Collections;

public class opendoor : MonoBehaviour {
	public Transform updoor;
	public Transform downdoor;
	public bool NeedKey;
	bool Dooropen;
	// Use this for initialization
	void Start () {
		Dooropen = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D (Collider2D other){
		if (other.tag == "Player") {
			
			if (NeedKey == true) {
				
				if (other.GetComponent <collect_item> ().HasKey == true) {
					porta (0.65f);
				}
			} 
			else {
				porta (0.65f);
			}
			}
	}
	void OnTriggerExit2D (Collider2D other){
		if (other.tag == "Player") {
			if (Dooropen == true) {
				porta (-0.65f);
			}
			}
	}
	void porta (float openingsize){
		updoor.Translate (new Vector3(0,openingsize,0));
		downdoor.Translate (new Vector3(0,-openingsize,0));
		Dooropen = !Dooropen;
	}

}
